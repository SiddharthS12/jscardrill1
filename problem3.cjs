// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.//

function problem3(inventory) {
  let inventory_array = inventory;
  inventory_array.sort((a, b) => {
    if (a.car_model < b.car_model) {
      return -1;
    } else if (a.car_model > b.car_model) {
      return 1;
    }
    return 0;
  });
  return inventory_array;
}

module.exports = { problem3 };
