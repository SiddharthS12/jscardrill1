// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:
// "Car 33 is a *car year goes here* *car make goes here* *car model goes here*"
// console.log(__dirname);
const problem1 = (inventory = [], findId) => {
  const empty= []
  if (!Array.isArray(inventory)) {
    return empty
  }
  if (inventory.length === 0) {
    return empty
  }
  if (findId === undefined) {
    return empty
  }
  if (typeof findId !== "number") {
    return empty
  }

  for (let index = 0; index < inventory.length; index++) {
    let obj = inventory[index];

    if (obj["id"] === findId) {
      return obj;
    }
  }
  return null;
};

module.exports = problem1;
