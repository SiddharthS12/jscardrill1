// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.
//
function problem4(inventory) {
  let inventory_array = inventory;
  let years_array = [];
  for (let index = 0; index < inventory_array.length; index++) {
    years_array.push(inventory_array[index]["car_year"]);
  }
  console.log(years_array);
  return years_array;
}

module.exports = { problem4 };
