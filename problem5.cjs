// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.
//

function problem5(inventory) {
  let inventory_array = inventory;
  let cars_before_2000 = [];
  for (let index = 0; index < inventory_array.length; index++) {
    if (inventory_array[index]["car_year"] < 2000) {
      cars_before_2000.push({
        car_name: inventory_array[index]["car_make"],
        car_year: inventory_array[index]["car_year"],
      });
    }
  }
  return cars_before_2000;
}
module.exports = { problem5 };
