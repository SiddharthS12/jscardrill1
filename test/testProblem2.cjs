const inventory = require("../Inventory.cjs");
const { problem2 } = require("../problem2.cjs");

const result_obj = problem2(inventory);
// console.log(result_obj);

console.log(`Last Car is a ${result_obj.car_make} ${result_obj.car_model}`);
