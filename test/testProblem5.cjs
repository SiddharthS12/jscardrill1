const inventory = require("../Inventory.cjs");
const { problem5 } = require("../problem5.cjs");

const result_obj = problem5(inventory);
console.log(result_obj);
console.log(`Number of Cars older than the year are:-`, result_obj.length);
