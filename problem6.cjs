// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.
//

function problem6(inventory) {
  let inventory_array = inventory;
  let BMW_AUDI_CARS = [];
  for (let index = 0; index < inventory_array.length; index++) {
    if (
      inventory_array[index]["car_make"] == "BMW" ||
      inventory_array[index]["car_make"] == "Audi"
    ) {
      BMW_AUDI_CARS.push(inventory_array[index]);
    }
  }
  return BMW_AUDI_CARS;
}
module.exports = { problem6 };
